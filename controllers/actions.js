
const jsonWebToken = require('jsonwebtoken');
const dataBase = require('../dataBase/db');
const { promisify } = require('util');

const dotenv = require('dotenv');
dotenv.config({ path: './.env'});

exports.act = (request, response) => {
    let keys = Object.keys(request.body);
    let button = keys.pop();
    let values = Object.values(request.body).slice(0, -1);
    let result = [];

    let cookies = request.cookies;
    let userId;
    jsonWebToken.verify(cookies.cookie, process.env.JWT_SECRET, (error, decoded) => {
        userId = decoded.id;
    });    

    for(let i = 0; i < values.length; i++) {
        if(values[i] == 'on') {
            if(userId == keys[i]) {
                response.cookie('cookie', 'loggedout', {
                    expires: new Date(Date.now() + 10 * 1000),
                    httpOnly: true
                });
            }
            result.push(keys[i]);   
        }
    }


    if(button == 'delete') {
        for(let i = 0; i < result.length; i++) {
            dataBase.launch.query('Delete from users where id = $1', [result[i]], async (error, result) => {
                if(error) {
                    console.log(error);
                }
            });
        }
    }

    if(button == 'block') {
        for(let i = 0; i < result.length; i++) {
            dataBase.launch.query("Update users set isBanned = 'true' where id = $1", [result[i]], async (error, result) => { //isBanned true false
                if(error) {
                    console.log(error);
                }
            });
        }
    }

    if(button == 'unblock') {
        for(let i = 0; i < result.length; i++) {
            dataBase.launch.query("Update users set isBanned = 'false' where id = $1", [result[i]], async (error, result) => {
                if(error) {
                    console.log(error);
                }
            });
        }
    }
    response.status(201).redirect("/");
}
