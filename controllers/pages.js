const express = require('express');
const controller = require('../controllers/auth');

const router = express.Router();

router.get('/', controller.isLogged, (request, response) => {
    response.render('index', {
      user: request.user
    });
});
  
router.get('/login', (request, response) => {
    response.render('login');
});
  
router.get('/register', (request, response) => {
    response.render('register');
});

module.exports = router;