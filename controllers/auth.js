const { passCommandToAction } = require('commander');
const { response } = require('express');
const jsonWebToken = require('jsonwebtoken'),
      bcrypt = require('bcryptjs'),
      dataBase = require('../dataBase/db');
const { promisify } = require('util');

exports.register = (requare, response) => {

    const { username, email, password, confirmation } = requare.body;

    dataBase.launch.query('Select email from users where email = $1', [email], async (error, data) => {
        if (error) 
            console.log(error);
        if (data.length > 0) {
            return response.render('register', {
               message: 'This email is already registrated' 
            });
        }else if (password !== confirmation) {
            return response.render('register', {
                message: "Password isn't correct"
            });
        }
    
    let passwordHash = await bcrypt.hash(password, 8);

    dataBase.launch.query('Insert into users(username, email, password) values($1, $2, $3)', [username, email, passwordHash], (error, result) => {
        if (error) {
            console.log(error)
        } else {
            dataBase.launch.query('select id from users where email = $1', [email], (error, result) => {
                if (error) {
                    console.log(error)
                } 
                const id = result.rows[0].id;
                const token = jsonWebToken.sign({ id }, process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_EXPIRES_IN
                });
                const cookieOptions = {
                    expires: new Date(
                        Date.now() + process.env.COOKIE_EXPIRES_IN * 86400 * 100 
                    ),
                    httpOnly: true
                };
                response.cookie('cookie', token, cookieOptions);
                response.status(201).redirect('/');
            });
        }
    });
 });
}

exports.login = async (request, response, next) => {
    const { email, password } = request.body;

    if (!email || !password) {
        return response.status(400).render('login' , {
           message: 'Please enter email and password' 
        });
    }

    dataBase.launch.query('Select * from users where email = $1', [email], async (error, result) => {
        if (error) {
            console.log(error)
        }
        if (result.length == 0 || undefined) {
            return response.status(401).render('login', {
                message: 'Please, re-enter password and email'
            });
        }

        const matches = await bcrypt.compare(password, result.rows[0].password);
        if (!result || !matches) {
            return response.status(401).render('login', {
                message: 'Incorrect password or email'
            });
        } else {
            if (result.rows[0].isbanned == 'false') { 
                const id = result.rows[0].id;
                const token = jsonWebToken.sign({ id }, process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_EXPIRES_IN
                });

                const cookieOptions = {
                    expires: new Date(
                        Date.now() + process.env.COOKIE_EXPIRES_IN * 86400 * 100 
                    ),
                    httpOnly: true
                };
                response.cookie('cookie', token, cookieOptions); 
                response.status(201).redirect('/');
            } else {
                return response.status(401).render('login', {
                    message: 'You are banned!'
                });
            }
        } 
    });
};

exports.isLogged = async (request, response, next) => {
    console.log(request.cookies.cookie)
    if (request.cookies.cookie) {
        try {
            const decoded = await promisify(jsonWebToken.verify)(
                request.cookies.cookie,
                process.env.JWT_SECRET
            );
            console.log(`\n\n ${decoded.id} \n\n`)

            dataBase.launch.query('Select * from users', [], (error, result) => {
                if (!result) {
                    return next();
                }
                request.users = result.rows;
            });

            dataBase.launch.query('select * from users where id = $1', [decoded.id], (error, result) => {
                if (!result) {
                    return next();
                }
                request.user = result.rows[0];
                return next();
            });
        } catch (error) {
            return next();
        }
    } else {
        next();
    }
};
