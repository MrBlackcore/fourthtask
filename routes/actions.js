const express = require('express');
const controller = require('../controllers/actions');

const router = express.Router();

router.post('/act', controller.act);

module.exports = router;