const express = require('express'), 
      path = require('path'), 
      cookieParser = require('cookie-parser'), 
      app = express();

const dataBase = require('./dataBase/db');

const publicDir = path.join(__dirname, './public');
app.use(express.static(publicDir));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cookieParser());
app.set('view engine', 'hbs');

dataBase.launch.connect((error) => {
    if (error)
        console.log(error);
    else
        console.log('connected to dataBase');
});

app.use('/', require('./routes/pages'));
app.use('/auth', require('./routes/auth'));
app.use('/actions', require('./routes/actions'));

app.listen(process.env.PORT || 5000, () => {
    console.log('Server is ready')
});
